﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Sockets;
using System.Net;

namespace RocketCore.Engine.Replication
{
    static class Snapshot
    {
        #region CALLABLE SNAPSHOT FUNCTIONS

        internal static StatusCodes BackupMasterSnapshot()
        {
            Client.Start(); //запускаем репликационное соединение и поток, если ещё не запущены

            Queues.slave_queue.Enqueue(new FuncCallReplica((int)FuncIds.BackupMasterSnapshot));

            return StatusCodes.Success;
        }

        internal static StatusCodes RestoreSnapshot(bool current)
        {
            if (!Flags.market_closed) return StatusCodes.ErrorMarketOpened; //разрешаем восстановление снэпшота только в случае закрытого рынка

            int func_id = current ? (int)FuncIds.RestoreSlaveSnapshot : (int)FuncIds.RestoreMasterSnapshot;
            Queues.slave_queue.Enqueue(new FuncCallReplica(func_id));

            return StatusCodes.Success;
        }

        internal static StatusCodes RestoreMasterSnapshot()
        {
            return RestoreSnapshot(false);
        }

        internal static StatusCodes RestoreSlaveSnapshot()
        {
            return RestoreSnapshot(true);
        }
        
        #endregion

        #region SERVICE SNAPSHOT FUNCTIONS

        internal static bool BackupMasterSnapshotInner(TcpClient client)
        {
            bool _sent = SocketIO.WriteStrCmd(client, "bckm");

            if (_sent)
            {
                bool _serialized = SocketIO.SerializeCore(client);

                if (_serialized)
                {
                    Console.WriteLine("SLAVE CORE: master snapshot serialized");
                    Pusher.NewSnapshotOperation((int)SnapshotOperations.BackupMaster, (int)StatusCodes.Success, DateTime.Now); //сообщение о новой операции со снэпшотом
                    return true;
                }
                else
                {
                    Console.WriteLine("==Core serialization error==");
                    Pusher.NewSnapshotOperation((int)SnapshotOperations.BackupMaster, (int)StatusCodes.ErrorSnapshotBackupFailed, DateTime.Now); //сообщение о новой операции со снэпшотом
                    return false;
                }
            }
            else
            {
                Console.WriteLine("SLAVE CORE: failed to send a cmd (dc)");
                return false;
            }
        }

        internal static bool RestoreSnapshotInner(TcpClient client, bool current)
        {     
            if (!current)
            {
                Queues.slave_queue.Clear(); //очистка текущих FC в stdf_queue 
                Queues.stdf_queue.Clear(); //очистка текущих FC в stdf_queue 
                Queues.prdf_queue.Clear(); //очистка текущих FC в prdf_queue
            }

            string cmd = current ? "rsts" : "rstm";
            bool _sent = SocketIO.WriteStrCmd(client, cmd);

            if (_sent)
            {
                bool _deserialized = SocketIO.DeserializeCore(client);
                int op_code = current ? (int)SnapshotOperations.RestoreSlave : (int)SnapshotOperations.RestoreMaster;

                if (_deserialized)
                {
                    Console.WriteLine("SLAVE CORE: snapshot deserialized");
                    Pusher.NewSnapshotOperation(op_code, (int)StatusCodes.Success, DateTime.Now); //сообщение о новой операции со снэпшотом
                    return true;
                }
                else
                {
                    Console.WriteLine("==Core deserialization error==");
                    Pusher.NewSnapshotOperation(op_code, (int)StatusCodes.ErrorSnapshotRestoreFailed, DateTime.Now); //сообщение о новой операции со снэпшотом
                    return false;
                }
            }
            else
            {
                Console.WriteLine("SLAVE CORE: failed to send a cmd (dc)");
                return false;
            }
        }

        #endregion
    }
}
