﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class FixRestartMsg : IJsonSerializable
    {
        private int status_code;
        private DateTime dt_made;

        internal FixRestartMsg(int status_code, DateTime dt_made) //конструктор сообщения
        {
            this.status_code = status_code;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewFixRestart, status_code, dt_made);
        }
    }
}
