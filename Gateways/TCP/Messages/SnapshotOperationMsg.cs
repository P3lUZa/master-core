﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class SnapshotOperationMsg : IJsonSerializable
    {
        private int op_code;
        private int status_code;
        private DateTime dt_made;

        internal SnapshotOperationMsg(int op_code, int status_code, DateTime dt_made) //конструктор сообщения
        {
            this.op_code = op_code;
            this.status_code = status_code;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewSnapshotOperation, op_code, status_code, dt_made);
        }
    }
}
